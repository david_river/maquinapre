package com.certificacion.controller;

import java.util.HashMap;
import java.util.List;

import com.certificacion.model.Maquina;
import com.certificacion.model.Maquina.Tipo;
import com.certificacion.repository.IMaquinaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("maquina")
@CrossOrigin
public class MaquinaController {

    @Autowired
    IMaquinaRepository rMaquina;

    @GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Maquina> getAll() {
        return (List<Maquina>) rMaquina.findAll();

    }

    @GetMapping(value = "save", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public HashMap<String, String> save(@RequestParam String nombre,
            @RequestParam String descripcion, @RequestParam Tipo tipo) {
        Maquina maquina = new Maquina();
        maquina.setNombre(nombre);
        maquina.setDescripcion(descripcion);
        maquina.setTipo(tipo);

        HashMap<String, String> jsonReturn = new HashMap<>();

        try {
            rMaquina.save(maquina);

            jsonReturn.put("Estado", "OK");
            jsonReturn.put("Mensaje", "Registro guardado");

            return jsonReturn;
        } catch (Exception e) {

        }
        return jsonReturn;
    }

    @GetMapping(value = "update", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public HashMap<String, String> saves(@RequestParam Long id, @RequestParam String nombre,
            @RequestParam String descripcion, @RequestParam Tipo tipo) {
        Maquina maquina = new Maquina();
        maquina.setCodigo(id);
        maquina.setNombre(nombre);
        maquina.setDescripcion(descripcion);
        maquina.setTipo(tipo);

        HashMap<String, String> jsonReturn = new HashMap<>();

        try {
            rMaquina.save(maquina);

            jsonReturn.put("Estado", "Ok");
            jsonReturn.put("Mensaje", "Registro guardado");
            return jsonReturn;
        } catch (Exception e) {

            jsonReturn.put("Estado", "Error");
            jsonReturn.put("Mensaje", "Registro no guardado");

            return jsonReturn;
        }
    }

    /* Listar registros por id de la tabla de maquina en el modal de editar */
    @GetMapping(value = "get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Maquina getMethodName(@PathVariable Long id) {
        return rMaquina.findById(id).get();
    }

      //ELIMINAR
      @GetMapping(value = "delete/{id}")
      @ResponseBody
      public HashMap<String, String> delet (@PathVariable Long id) {
  
          HashMap<String, String> jsonReturn = new HashMap<>();
  
          try {
              Maquina maquina = rMaquina.findById(id).get();
              rMaquina.delete(maquina);
  
              jsonReturn.put("Estado", "OK");
              jsonReturn.put("Mensaje", "Registro eliminado");
  
              return jsonReturn;
          } catch (Exception e) {
  
              jsonReturn.put("Estado", "Error");
              jsonReturn.put("Mensaje", "Registro no eliminado");
  
              return jsonReturn;
          }
      }

}
