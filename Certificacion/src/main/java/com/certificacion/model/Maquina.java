package com.certificacion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "maquina")
public class Maquina {

    public enum Tipo {
        A, B, C
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigo;
    
    private String nombre;
    private String descripcion;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 30, columnDefinition = "ENUM('A', 'B', 'C')")
    private Tipo tipo;

    /* Constructor Vacio */
    public Maquina() {

    }

    public Maquina(Long codigo, String nombre, String descripcion, Tipo tipo) {
        super();
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipo = tipo;
    }

    /* Se crean los Getters y los Setters */
    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public Long getCodigo() {
        return this.codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Tipo getTipo() {
        return this.tipo;
    }
}
