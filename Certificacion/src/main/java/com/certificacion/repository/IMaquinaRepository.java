package com.certificacion.repository;

import com.certificacion.model.Maquina;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/* Se crea el repository para la interface de maquina */
@Repository
public interface IMaquinaRepository extends CrudRepository<Maquina, Long>{
    
}