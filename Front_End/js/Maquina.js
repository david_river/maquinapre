let maquina = {
    id: 0
}
/* Se llama la funcion del set Id del maquina */
function setIdMaquina(id) {
    maquina.codigo = id
}

$(document).ready(inicio);
/* Se inicializa la funcion para cargar los datos de la tabla  */
function inicio() {
    /* Cargar los datos de la tabla del maquina  */
    cargarDatos();
    /* Evento click con el boton guardar para el nuevo registro del maquina */
    $("#btnGuardar").click(guardar);
    /* Evento click con el boton eliminar del maquina */
    $("#btnEliminar").click(function () {
        eliminar(maquina.codigo)
    });
    /* Evento click para modificar del maquina */
    $("#btnActualizar").click(modificar);
    /* Evento click para resetear del maquina */
    $("#btnCancelar").click(reset);
}
/* Funcion de resetear de la tabla del maquina */
function reset() {
    /* $("#codigo").val(null); */
    $("#nombre").val(null);
    $("#descripcion").val(null);
    /* $("#tipo").val(null); */

   /*  $("#codigo2").val(null); */
    $("#nombre2").val(null);
    $("#descripcion2").val(null);
    /* $("#tipo2").val(null); */
}

//CARGANDO DATOS A TABLA MAQUINA
function cargarDatos() {
    /* Peticion ajax a back_end */
    $.ajax({
        url: "http://localhost:8080/maquina/all",
        method: "Get",
        data: null,
        success: function (response) {
            $("#datos").html("");
            /* Procesando datos de la tabla */
            /* Y agregar los valores obtenidos de la respuesta del tbody */
            for (let i = 0; i < response.length; i++) {
                $("#datos").append(
                    "<tr>" +
                    "<td><strong>" + response[i].codigo + "</strong></td>" +
                    "<td><strong>" + response[i].nombre + "</strong></td>" +
                    "<td><strong>" + response[i].descripcion + "</strong></td>" +
                    "<td><strong>" + response[i].tipo + "</strong></td>" +
                    "<td>" +
                    "<button onclick='cargarRegistro(" + response[i].codigo +
                    ")'type='button' class='btn btn-info ml-3 mt-1' data-toggle='modal' data-target='#editar'> <strong>Editar</strong></button>" +
                    "<button onclick='setIdMaquina(" + response[i].codigo +
                    ");' type='button' class='btn btn-danger ml-3 mt-1' data-toggle='modal' data-target='#eliminar'> <strong>Eliminar</strong></button>" +
                    "</td>" +
                    "</tr>"
                )
            }
        },
        /* Se muestra una alerta si algo esta mal en la peticion */
        error: function (response) {
            alert("Error: " + response);
        }
    });
}
/* Funcion guardar  nuevo registro del maquina */
function guardar(response) {
       $.ajax({
        url: "http://localhost:8080/maquina/save",
        method: "Get",
        data: {
            nombre: $("#nombre").val(),
            descripcion: $("#descripcion").val(),
            tipo: $("#tipo").val()
        },
        success: function () {
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}
/* Funcion de eliminacion del registro */
function eliminar(id) {
    $.ajax({
        url: "http://localhost:8080/maquina/delete/" + id,
        method: "Get",
        success: function () {
            cargarDatos();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}
/* Funcion para cargar los registros */
function cargarRegistro(id) {
    $.ajax({
        url: "http://localhost:8080/maquina/get/" + id,
        method: "Get",
        success: function (response) {
            $("#id2").val(response.codigo)
            $("#nombre2").val(response.nombre)
            $("#descripcion2").val(response.descripcion)
            $("#tipo2").val(response.tipo)
        },
        error: function (response) {
            alert("Error en la peticion " + response);
        }
    })
}
/* Funcion de modificar de la tabla del maquina */
function modificar() {
    var id = $("#id2").val();
    $.ajax({
        url: "http://localhost:8080/maquina/update",
        method: "Get",
        data: {
            id: id,
            nombre: $("#nombre2").val(),
            descripcion: $("#descripcion2").val(),
            tipo: $("#tipo2").val()
        },
        success: function (response) {
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response);
        }
    });
}